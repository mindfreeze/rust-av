+++
title = "The rav1e Community"
description = "Get help, discuss problems, and join the fun"
+++

# About

[AV1](https://en.wikipedia.org/wiki/AV1) is an open and royalty-free video coding format designed by the Alliance for Open Media, concurrent with [HEVC](https://en.wikipedia.org/wiki/HEVC) (H.265). AV1 enables to stream video content at a reduced bitrate without loss of visual quality.

[rav1e](https://www.youtube.com/watch?v=ytsRYKQc6kQ) is an encoder written in [Rust](https://www.rust-lang.org/), developped by [Mozilla](https://research.mozilla.org/av1-media-codecs/)/[Xiph](https://xiph.org/). As such, it takes an input video and encodes it to produce a valid AV1 bitstream.
# Communication

You can join us at #daala on Freenode Network in IRC. If you do not have IRC Set up, you can use from [web browser](http://webchat.freenode.net/?channels=%23daala), where you're welcome to ask any doubts related to Video Technology, follow our developments, report bugs related to rav1e and also av1, you also welcome to suggest any feature requests which you feel its worth.

You are encouraged to attend the weekly progress meetings by installing and using [Mumble](http://wiki.mumble.info/).

The address is `mf4.xiph.org` and the port is `64738`.

They occur on Tuesdays at [9 AM Pacific Time](https://www.timeanddate.com/worldclock/fixedtime.html?msg=Daala+Weekly+Meeting&iso=20190801T09&p1=1241). 

The meeting agenda can be found at this [Etherpad](https://public.etherpad-mozilla.org/p/daala-weekly-meeting).

# Issues

Currently we are using Github is used for tracking issues. The primary location for these is the main
[rav1e repository](https://github.com/xiph/rav1e/issues).

These repositories also contain milestones for upcoming *major* releases, which
are currently used to track breaking changes that might be considered for the
next major release. There are also some set of projects which are on progess you can track.

# Get Involved

Are you interested in getting involved with the code and participating in
our development? Join us over the IRC.

# Contributors

The development of rav1e would not be possible without help from volunteers around the [globe](https://github.com/xiph/rav1e/graphs/contributors).